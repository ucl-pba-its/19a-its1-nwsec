---
title: 'ITS1 NW security'
subtitle: 'Exercises'
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITS1 NW security, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References

* [Weekly plans](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_weekly_plans.pdf)
