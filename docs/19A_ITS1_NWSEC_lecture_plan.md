---
title: '19A PBa IT sikkerhed'
subtitle: 'Fagplan for Netværks- og kommunikationssikkerhed'
main_author: 'Morten Bo Nielsen/MON'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'Fagplan for Netværks- og kommunikationssikkerhed'
skip-toc: true
filename: 19A-ITS1-NWSEC_lecture_plan
semester: 19A
---

# Introduktion

I dette fag vil teknologier og metoder bliver præsenteret, og der vil blive arbejdet med det.

Det er tænkt som introduktion og name-dropping, og den studerende skal således selv supplere op og kombinere med projekt faget for at få maksimal udbytte.


Faget er på 10 ECTS point.

# Læringsmål

Viden
Den studerende har viden om og forståelse for:

* Netværkstrusler
* Trådløs sikkerhed
* Sikkerhed i TCP/IP
* Adressering i de forskellige lag
* Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl)
* Hvilke enheder, der anvender hvilke protokoller
* Forskellige sniffing strategier og teknikker
* Netværk management (overvågning/logning, snmp)
* Forskellige VPN setups
* Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).

Færdigheder
Den studerende kan:

* Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
* Teste netværk for angreb rettet mod de mest anvendte protokoller
* Identificere sårbarheder som et netværk kan have.

Kompetencer
Den studerende kan håndtere udviklingsorienterede situationer herunder:

* Designe, konstruere og implementere samt teste et sikkert netværk
* Monitorere og administrere et netværks komponenter
* Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)
* Opsætte og konfigurere et IDS eller IPS.



Se [Studieordningen sektion 2.4](https://www.ucl.dk/link/3a5f054541da4bbca44c677abf1ee8c3.aspx?nocache=77112065-b434-4e74-a5b8-336327053716)

# Lektionsplan

| Teacher | Week | Aktivitet |  
| :--: | :---:  | :--- |
| MON | 36 | Introduktion til faget. Tooling up: VMs og linux.|
| MON | 37 | Del 1: Forstå netværk - OSI modellen og network hardware: routere, switch, AP, 802.3, 802.11, firewalls |  
| MON | 38 | Netværkstopologi: subnets, IP adresser, VLANs |
| MON | 39 | Bygge netværk: Virtuelle netværk, physiske netværk |
| MON | 40 | Netværks protokoller: Wireshark, sniffe trafik, HTTP, HTTPS, m. fl. |
| MON | 41 | Monitorering: SMNP, syslog, ELK, librenms, måske nagios |
| MON | 42 | Efterårsferie: skemafri, ikke ferie |
| MON | 43 | Network services og applikationer: DNS, ARP, DHCP. Webapps, APIer. MON har eksamener på ITT, så der er måske noget der |  
| MON | 44 | Del 2: Sikkerhed på netværk - OSI modellen igen: Sikkerhed på lag 1, 2, 4 og 5. |  
| MON | 45 | Hardware sikkerhed på lag 1 og 2: wireless sniffing, 802.1x |  
| MON | 46 | Hardware sikkerhed lag 3 og 4: firewall rules, vSRX, pfsense |
| MON | 47 | Hardware sikkerhed lag 5: application layer security. Palo Alto NG firewalls. |
| MON | 48 | Topologier: segmentation, DMZ, non-routable networks, lateral movement |
| MON | 49 | Passive monitoring: sniffing, logs, DNS/NS |
| MON | 50 | Active monitoring, scanning: shodahq, nmap |
| MON | 51 | Compromising networks: red teaming, pen testing |  
| MON | 52 | Juleferie |
| MON | 01 | Juleferie |
| MON | 02 | Recap |
| MON | 03 | Exams |
