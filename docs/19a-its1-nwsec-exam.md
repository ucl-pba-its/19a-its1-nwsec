---
title: '19A PBa IT sikkerhed'
subtitle: 'Eksamensprocedure for Netværks- og kommunikationssikkerhed'
main_author: 'Morten Bo Nielsen/MON'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'Eksamensprocedure nwsec'
skip-toc: true
---

# Baggrund

Tidligere dokumenter

* [Fagplanen](https://ucl-pba-its.gitlab.io/19-its-docs/19A-ITS-netværks--og-kommunikationssikkerhed.pdf)
* [Ugeplaner](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_weekly_plans.pdf) (se også [hjemmesiden](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/))
* Studieordningen, [National del](https://www.ucl.dk/link/3a5f054541da4bbca44c677abf1ee8c3.aspx?nocache=96d4162e-02ec-43e7-bad6-62069d54eef5) og [institutionsdel](http://esdhweb.ucl.dk/D19-1181322.pdf?nocache=d07d3775-ea98-4910-8b53-a3b5e03e2b21)

# Eksamen

Mandag d. 14. og tirsdag d. 15. januar 2020 er afsat. Det er usikkert om vi kan få styr på den specifikke tidsplan før juleferie, ellers vil den foreligge først i de nye år.

Fra studieordningen

```
Prøven er en individuel, mundtlig prøve med udgangspunkt i to trukkede emner.
Alle emner, der kan trækkes til eksamen, er udleveret til de studerende mindst 14 dage før eksamen, så de
studerende har mulighed for at forberede sig på alle eksamensspørgsmål. Ud af de på forhånd udleverede emner,
trækker den studerende to emner inde til eksamen. Der er ingen forberedelse på selve dagen.

Den individuelle, mundtlige eksamen har et omfang af 30 minutter inkl. votering, heraf:

1. 25 minutter til fremlæggelse af og eksamination i de to trukkede emner
2. 5 min til votering.
```
Karakterer efter 7-trins skalaen og der er ekstern censur.

# Undervisers forventning til eksamenen

Til eksamenen skal den studerende vise at læringsmålene er opfyldt. Der er som bekendt langt mellem 02 og 12, og studerende kan vælge en strategi der passer til den enkeltes temperament, evner og stil.

Generelt set, så ønsker vi at se og høre at den studerende har en god forståelse for hvordan forskellige typer data flyder rundt i et netværk og kan relatere det til sikkerhed.

Det forventes at den studerende viser eksempler indefor de trukne emner som belyser både en teoretisk forståelse af emnet, samt praktisk anvendelse, hvis muligt. Den sidste del vil ofte være en form for live demonstration.

Eftersom kommunikation er en vigtig del af uddannelsen forventes det også at præsentationer og demonstrationer er gearet til det forventede publikum, ie. underviser og censor.

# Emneliste

Nedenstående er listen over emner som kan trækkes.

* OSI modellen: Indhold og relevans i et sikkerhedsperspektiv
* Load balancers: Beskrivelse og funktion i et netværk.
* Subnets og routning: Routable, non-routable, DMZ
* HTTPS og DNS: Resolve hostnavne, webservere med flere virtual hosts
* Monitorering (funktionel): Beskriv de 3 typer og hvad de bruges til; rød/grøn, grafer og logs.
* Sniffe trafik: Hvilke muligheder er der? Hvad kan man forvente at opsamle?
* Firewalls: Funktion i netværket, egenskaber
* Aktiv monitorering/scanning: Egenskaber, upassende/passende brug
