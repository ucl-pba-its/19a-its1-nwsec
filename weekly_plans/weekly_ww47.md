---
Week: 47
Content:  Application layer security
Material: See links in weekly plan
Initials: MON
---

# Uge 47

Sikkerheds på applikationslaget. Dette er når vi åbner op for protokoller og forholder os til hvad der foregår indeni, såsom at opdage et HTTP kører på en non-standard port.

## Mål for ugen

Praktiske og læringsmål for ugen er

### Praktiske mål

* Den studerende har en vSRX oppe at køre

### Læringsmål

Den studerende kan

* forklare hvad en firewall gør på lag 3 og 4, og hvordan den bruges til at højne sikkerheden.
* design og konfigurere simple regler i en firewall

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15  | MON starter op |
| 10:30 | opgave 1 gennemgang |

MON har møder om eftermiddagen.

Links:

* Video on firewall topologies is on [youtube](https://www.youtube.com/watch?v=bvdbH5AUMe0)
* associated pdf is in [docs](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/2019-11-11-Note-13-45-firewalls.pdf)

## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
