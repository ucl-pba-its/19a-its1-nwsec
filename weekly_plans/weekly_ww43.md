---
Week: 43
Content:  Network services og applikationer
Material: See links in weekly plan
Initials: MON
---

# Uge 43

Undervisning denne uge faldt ud.

Emner som DNS, ARP, DHCP. Webapps og APIer vil vi flette ind i anden sammenhæng.
