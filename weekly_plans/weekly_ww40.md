---
Week: 40
Content:  Netværksprotokoller
Material: See links in weekly plan
Initials: MON
---

# Uge 40

Der er mange netværksprotokoller. 3. semester er i gang med 5 ECTS om det, så vi kommer til at ridse lidt i overfladen.

## Mål for ugen

Pratical and learning goals for the period is as follows

### Praktiske mål

Ingen.

### Læringsmål

* Den studerende kan forklare HTTP(S) og DNS
* Den studerende kan bruge relevante analyse værktøjer til at undersøge netværkstrafikken.

## Leverancer

Ingen


## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | MON gennemgår HTTP og lidt HTTPS workshop style |
| 9:00 | pause :-) |
| 9:15 | fortsat |
| 10:00 | mere pause |
| 10:15 | MON om DNS, også workshop style |
| 11:00 | JE: reverse nginx proxy  |
| 11:30 | Frokost |
| 12:15 | JE: fortsat  |

Links:

* JE opgave om [reverse proxy](https://ucl-its-projects.gitlab.io/reverse-proxy/#0)

## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
