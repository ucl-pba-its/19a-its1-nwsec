---
Week: 37
Content:  OSI og hardware
Material: See links in weekly plan
Initials: MON
---

# Week 37

Denne dag falder ud pga. summergames.


## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

None

### Learning goals

* Den sturende kan forklare OSI modellen og de tilhørende lag
* Den studerende kan placere hardware på de forskellige lag i OSI modellen.
* Den studerende kender til Feynman metoden for selv-evaluerende selv-studie.

## Deliverables

* Exercise 1 afleveres på itslearning.


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Friday

Tidsplanen:

| Time | Activity |
| :---: | :--- |
| 8:15 | Summergames |


## Hands-on time

### Exercise 1

Brug Feynman metoden til at få styr på OSI modellen.

1. Skrive en halv side om "OSI og hardware"
2. Gennemlæs det som du har skrevet, og understreg de 5 steder for du føler dig svagest.
3. Læs op på de 5 punkter og/eller spørg underviser eller andre.
4. Iterér igen, dvs. udfør punkt 1-3 igen.
5. Put begge tekster, inkl. understregning, i samme dokument
6. Aflever på itslearning

## Comments
* Vi laver nok en del om torsdagen
* Feynman metoden er mega cool. se [her](https://www.youtube.com/watch?v=tkm0TNFzIeg) eller [her](https://collegeinfogeek.com/feynman-technique/)
