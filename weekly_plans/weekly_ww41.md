---
Week: 41
Content:  Monitorering
Material: See links in weekly plan
Initials: MON
---

# Uge 41

TBA

## Mål for ugen

Pratical and learning goals for the period is as follows

### Praktiske mål

* evaluering - Der burde ligge en survey exact mail

### Læringsmål

* Den studerende kan forklare de tre forskellige typer af monitorering.


## Leverancer

Ingen


## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | MON er til eksamenen. |
|  | Studerende arbejder selv. |
| 11:30 | Frokost |
| 12:30 | Opsamling og videre arbejde med opgaverne  |

Links:

* se billeder om monitorering på riot :-)

## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
