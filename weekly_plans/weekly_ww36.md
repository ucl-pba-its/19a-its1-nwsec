---
Week: 36
Content:  Fagopstart
Material: See links in weekly plan
Initials: MON
---

# Uge 36

Vi har en halv dag  denne uge, og så er der fredagsbar.

## Mål for ugen
Pratical and learning goals for the period is as follows

### Praktiske mål
* Ingen

### Læringsmål

* Den studerende ved hvad faget gårud på
* Den studerende kender til OSI modellen

## Leverancer

* Diskussion på klassen


## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)


### Fredag

Tidsplanen:

| Time | Activity |
| :---: | :--- |
| 8:15 | MON introducerer faget |
| 9:00 | MON introducerer OSI modellen |
| 9:30 | Opgave 1 |
| 10:45 | Opgave 1 gennemgang på klassen |
| 12:30 | Friday bar |


## Hands-on

### Opgave 1

OSI model har 7 lag.

1. Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen.
2. Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways.
   Hvad-com-helst med et netstik er ok at have med.

## Kommentarer
* Jeg bytter IT sikkerhed og Netværkssikkerhed, så det bliver hhv. mandag og fredag istedet for hhv. fredag og mandag.
* Links om OSI modellen se [her](https://www.comparitech.com/net-admin/osi-model-explained/) og video [her](https://www.youtube.com/watch?v=vv4y_uOneC0)
