---
Week: 38
Content:  Netværkstopologi
Material: See links in weekly plan
Initials: MON
---

# Week 38

Netværk er en vigtig del af netværks- og kommunikations sikkerhed.

I denne uge ser vi på hvordan man organiserer sig fysisk og logisk med netværk.

Vi ser funktionelt på lag 3

* ingen lag 2 (såsom mirror ports, STP og VLANs)
* ingen firewall regler og andre generelle sikkerhedstiltag (det kommer en anden dag)
* ingen VPN'er

og routing tager vi i detaljer en anden dag.

(ja, der name droppes lidt, hvis man fik lyst til at google ting)

## Goals of the week(s)

Pratical and learning goals for the period is as follows

### Practical goals

* Den studerende har et VM system med load balancer

### Learning goals

* Den studerende kan forklare fordele og ulemper ved forskellige topologier
* Den studerende kender til forskellige netværksenheder og deres brug


## Leverancer

Ingen


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Friday

Tidsplanen:

| Time | Activity |
| :---: | :--- |
| 8:15 | MON shows and explains some standard network topologies |
|      | DMZ, port forwards
| 9:00 | break :-) |
| 9:15 | Edge/core, high availability |
|      | load balancing basics layer 3 vs 7 |
| 10:30 | Joint effort: Exercise 1 load balancer |
| 11:30 | Frokost |
| 12:15 | JE continued  |

Links:

* [Port forwards and NAT](https://www.homenethowto.com/ports-and-nat/port-forward/)
* [Edge and core (and HA)](https://www.juniper.net/documentation/en_US/release-independent/nce/topics/concept/cs1-design-understanding.html)
* [Bornhack network (obsolete)](https://bornhack.gitlab.io/noc/2019/hardware/2019/08/12/network-plan.html)
* [Load balancing design](https://www.digitalocean.com/community/tutorials/5-common-server-setups-for-your-web-application)

## Hands-on time

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
