---
Week: 39
Content:  Bygge netværk
Material: See links in weekly plan
Initials: MON
---

# Uge 39

Sidste uge snakkede vi om netværks topologi og hvordan man strukturere sit netværk for at få redundans og (lidt) sikkerhed.

Denne uge ser vi på hvordan man rent praktisk indretter sig med IP adresser, routere og VLANs.


## Mål for ugen

Pratical and learning goals for the period is as follows

### Praktiske mål

* Den studerende har en minimal Debian headless server og en openbsd router.

### Læringsmål

* Den studerende kan designe et netværk ud fra simple specifikationer
* Den studerende kender til kommando linien på forskellige OS'er
* Den studerende kan forklare forskellene på fysiske og virtuelle enheder (interfaces, servere, LANs)


## Leverancer

Ingen


## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | MON laver en show'n'tell om IPs, routing og VLANs |
| 9:00 | pause :-) |
| 9:15 | MON fortsætter |
| 10:30 | Joint effort: Exercise 1 OSPF routed network |
| 11:30 | Frokost |
| 12:15 | JE continued  |

Links:

* [IP adresser](https://www.lifewire.com/what-is-an-ip-address-2625920)
* Private IP adresser er i [rfc 1918](https://tools.ietf.org/html/rfc1918)
* CIDR notation og netmask, se [rfc4632](https://tools.ietf.org/html/rfc4632)
* VLANs er defineret i [802.1q](https://en.wikipedia.org/wiki/IEEE_802.1Q)
* [Openbsd officielle hjemmeside](https://www.openbsd.org/), som også har en [FAQ](https://www.openbsd.org/faq/faq1.html)

## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
