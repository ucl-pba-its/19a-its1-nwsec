---
Week: 46
Content:  Lag 3 og 4
Material: See links in weekly plan
Initials: MON
---

# Uge 46

Ugens emne er sikkerhed på lag 3 og 4.


## Mål for ugen

Praktiske og læringsmål for ugen er

### Praktiske mål

* Den studerende har et lille veldokumenteret netværk oppe at køre

    Det skal bruges i næste uge.

### Læringsmål

Den studerende kan

* forklare hvad en next-gen firewal er og gør, og hvordan den bruges til at højne sikkerheden.
* design og konfigurere simple regler i en next-gen firewall

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15  | MON starter op |

MON tilgængelig hele dagen.

Links:

* None

## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
