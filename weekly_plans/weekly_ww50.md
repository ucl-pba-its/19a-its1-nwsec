---
Week: 50
Content:  Active monitoring
Material: See links in weekly plan
Initials: MON
---

# Uge 50

Aktiv monitorering kender vi at scanne. Dette er ofte støjende og nemt at opdage. Det er også et godt værktøj for en adminsitrator, som ønsker at vide hvad der er på deres netværk.


## Mål for ugen

Praktiske og læringsmål for ugen er som følger

### Praktiske mål

* Evaluering er gennemgået.

### Læringsmål

Den studerende kan

* Aktiv monitorering og scanne med passende værktøjer

## Leverancer

* Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15  | MON starter op |
| 8:30  | Studerende arbejder |
| 12:30 | MON dukker op igen. |
|       | Gennemgang af evaluering |


MON har eksamener om formiddagen, og skal gå omkring kl 8.30. Efter frokost er MON tilstede ca. 12.30 til 14.00.

Links:
* [CIS CSC20 #3 Continuous Vulnerability Management](https://www.cisecurity.org/controls/continuous-vulnerability-management/)
* SCAP - security content automation protocol: [wikipedia](https://en.wikipedia.org/wiki/Security_Content_Automation_Protocol), [NIST](https://csrc.nist.gov/projects/security-content-automation-protocol/)
* [openscap intro](https://www.open-scap.org/getting-started/)
* Red Hat "vi er for seje til openscap" [video](https://www.youtube.com/watch?v=xmTt0MvyYQ8)
* [Vulnerability management](https://www.dnsstuff.com/network-vulnerability-scanner) - (kildekritik: afsender ikke vetted)

## Hands-on tid

OBS! (med udråbstegn): Vi laver aktive ting på netværket ofte mod servere og services der kører. Så slå hjernen til og lav ikke en `hail mary` ting, hvis det ikke virker i første forsøg. Vi har mulighed for at forstyrre netværket og services - hvis vi vil være destruktive, så skal det være med vilje.

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

* Jeg overvejde at snakke om scanning og (blue team) inventory problemer når vi begynder på ipv6, men besluttede at det var mere end vi kunne nå. Se f.eks. [RFC 7707](https://tools.ietf.org/html/rfc7707) og [draft-ietf-opsec-v6-21](https://tools.ietf.org/html/draft-ietf-opsec-v6-21)
* [common criteria](https://www.commoncriteriaportal.org/) er også relevant, men ikke nu.
