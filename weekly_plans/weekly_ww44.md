---
Week: 44
Content:  Monitorering
Material: See links in weekly plan
Initials: MON
---

# Uge 44

Denne uge drejer sig om at sniffe traffik, og hvor og hvordan det gøres.

MON er fraværende.

## Mål for ugen

Praktiske og læringsmål for ugen er

### Praktiske mål

Ingen

### Læringsmål

Den studerende kan

* forklare forskellige sniffe strategier
* opstætte netværks monitorering

## Leverancer

Ingen


## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Fredag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15 | Studerende arbejder. |

Links:

* Introduction to sniffing on [youtube](https://youtu.be/b_XQ7Xg1Jek)
* Slides are on [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/2019-10-28-Note-21-09-sniffing-trafic.pdf)

## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

Ingen pt.
