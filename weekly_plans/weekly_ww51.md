---
Week: 51
Content:  Red teaming
Material: See links in weekly plan
Initials: MON
---

# Uge 51

I har allerede hørt om red teaming. Denne uge sætter vi lidt flere ord på og noget læsestof.


## Mål for ugen

Praktiske og læringsmål for ugen er som følger

### Praktiske mål

* Eksamensspørgsmål er gennemgået

### Læringsmål

Den studerende kan

* Forklare hvad red teaming er og hvorfor det er relevant

## Leverancer

* Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Torsdag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
| 8:15  | MON starter op |
|   | Gennemgang af NW sec eksamen |

MON er tilgængelig hele dagen.

Links:
* [Lidt om red teaming](https://resources.infosecinstitute.com/everything-you-need-to-know-about-red-teaming-in-2018/#gref)
* [CIS CSC 20 control #20](https://www.cisecurity.org/controls/penetration-tests-and-red-team-exercises/)
* Read the [CIS SME guide](https://www.cisecurity.org/wp-content/uploads/2017/09/CIS-Controls-Guide-for-SMEs.pdf), and think like a red teamer. Conclusions?
* [Threat modelling, attack trees, STRIDE, ...](https://insights.sei.cmu.edu/sei_blog/2018/12/threat-modeling-12-available-methods.html)
* [Schneier on attack trees](https://www.schneier.com/academic/archives/1999/12/attack_trees.html)
* [More on STRIDE](https://www.slideshare.net/GirindroPringgoDigdo/threat-modeling-using-stride)
* [cyber kill chain](https://www.sans.org/security-awareness-training/blog/applying-security-awareness-cyber-kill-chain)

## Hands-on tid

Opgaver er på [gitlab](https://ucl-pba-its.gitlab.io/19a-its1-nwsec/19A_ITS1_nwsec_exercises.pdf)

## Kommentarer

* [Lidt mere CIS RAM](https://halock.wistia.com/medias/xoiu9510es) - som er out-of-scope i dag.
