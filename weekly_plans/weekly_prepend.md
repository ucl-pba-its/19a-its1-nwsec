---
title: 'ITS1 NW security'
subtitle: 'Weekly plans'
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITS1 NW security, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programm for each week of the second semester project.
